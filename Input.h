/*
Name : Input.h
Author: Amit Malyala
Date : 21-05-2016
Description:
Read a input ascii file into a string.
Version: 0.1
*/

#ifndef INPUT_H
#define INPUT_H

#include "string"
#include "fstream"
#include <iostream>
// Read Input file into a std::string
void readFile(std::string & OriginalMessage);
#endif