## Decryption of a XOR cipher with unknown key

This program written in ISO C++ 11 tries to decrypt a message which contains english text encrypted with XOR encryption with a unknown key by 
generating all possible keys to decrypt the message to obtain original message.Each message that is generated with generated 
key is then parsed to gather all possible words which are in that message. For each decrypted message, a score is calculated based 
on how many dictionary words are contained in the decrypted message compared to the size of the decrypted message.Each word 
is checked with a dictionary database. The decryption continues until all keys are generated and scores of 
all decrypted messages are compared.The maximum word length in each decrypted messsage is upto nineteen bytes. 
In this project, only four char upper or lowercase alphabetic keys which decode the messsage can be detected. 
The original message should contain english words mostly. 

Place File.txt and WordsEn.txt in the same directory as executable.
Place English text with english words in File.txt. A four char alphabetic key can be assigned to the string "key" in DecryptEngine.cpp in 
function DecryptEngine(void).The program will run until it detects the correct key and the original message is decoded or a error message is 
printed.

The project compiles with TDM-GCC G++ under C++11 and Visual C++ 2017 under C++14. 

Features to add:
Add support for random 16 byte ascii keys
Decode cipher text faster by reducing the number of words in dictionary with commonly used english words.
Brute force words in a dictionary and xor them with encrypted message to detect key and decode encrypted message. Use key length of 5 to 50 characters.
Use all words in the wordlist to detect key and decrypt the message.

Author: Amit Malyala
Email: amit at malyala.net