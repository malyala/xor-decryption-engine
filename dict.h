/*
Module Name :  Dict.h
Author: Amit Malyala
Date : 25-05-2016
Description:
The dictionary module loads a dictionary and then enables search of word strings in the dictionary vector.
Initialize the dictionary as a vector of word strings, sort this vector and do binay search to find a word string in the dictionary.
This file contains function prototypes of the dict module.
*/
#ifndef DICT_H
#define DICT_H

#include "string"
#include "fstream"
#include "vector"
#include "std_types.h"

// Initialize dictionary , load dictionary word strings from a file into a vector.
void InitDict(void); 

// Search for a word string in a vector of strings 
BOOL SearchDict(const std::string& key);

// Search for a word string in a vector of strings 
BOOL LinearSearchDict(const std::string& key);

#endif 