/*
Name : Input.cpp
Author: Amit Malyala
Description:
Read a ascii file into a string.
Version: 0.1
*/
#include "Input.h"

// Read a fie into a std::string
// Declare file name to be read as Original Message.
// The file can also have a encrypted message which is to be decoded.
void readFile(std::string& OriginalMessage)
{
		std::string filename = "file.txt";
		std::ifstream myfile (filename);
		char c;
		if (myfile.is_open())
		{
			while (myfile.get(c ))  // For reading bytes
			{
				OriginalMessage+=c;
			}
			myfile.close();
	   }
	   else
	   {
	   	  std::cout << std:: endl << "File not found";
	   }
}