/*
License: 
Copyright <2016> <Amit Malyala>, All rights reserved. 
Please see license.txt for details.
*/

#include "DecryptMessage.h"

// Function to decrypt a message
std::string DecryptMessage(const std::string& Message,  const std::string& key)
{
	SINT32 Messagelength= Message.length();
	SINT32 Keylength= key.length();
	std::string DecryptedMessage=Message;
	SINT32 i=0;
	SINT32 j=0; 
	while(i<Messagelength)
	{
		if (j == Keylength)
		{
			j=0;
		}
		DecryptedMessage[i] = Message[i]^key[j];
		i++;
		j++;
    }
   return DecryptedMessage;
}