/*
Name :  DecryptEngine.h
Author: Amit Malyala
License: 
Copyright <2016> <Amit Malyala>, All rights reserved. 
Please see license.txt for details.
Description:
Header file which has references to function prototypes of other modules.
*/
#ifndef DECRYPTENGINE_H
#define DECRYPTENGINE_H

#include "EncryptMessage.h"
#include "DecryptMessage.h"
#include "Keygen.h"
#include "TextAnalysis.h"

#endif 