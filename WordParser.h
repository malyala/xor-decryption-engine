/*
Name :  Wordparser.h
Author: Amit Malyala
Date : 25-05-2016
License: 
Copyright <2016> <Amit Malyala>, All rights reserved. 
Please see License.txt for details.
Description:
This is a header file which contains function prototypes.
*/

#ifndef PARSER_H
#define PARSER_H

#include "Std_types.h"
#include <string>
#include <vector>

// Parser function to detect words or substrings in a decrypted Message. Add a vector argument for storing words extracted
void WordParser(const std::string& str, std::vector <std::string>& WordList);

#endif 