/*
Name :  TextAnalysis.h
Author: Amit Malyala
Date : 25-05-2016
License: 
Copyright <2016> <Amit Malyala>, All rights reserved. 
Please see License.txt for details.
Description:
The input to this module is a vector of strings which is analyzed by 
by text analysis module which checks each word if its in a dictionary database and calculates
a score of each decrypted message. 
*/
#ifndef TEXTANALYSIS_H
#define TEXTANALYSIS_H

#include "string"
#include "WordParser.h"
#include "dict.h"

// Write a typedef of type struct which is used to hold datasets.
// We'll create 10 such datasets to do text analysis of 10 Messages at a time until all decrypted messages 
// with generated keys are analyzed or an original message is found before that.
typedef struct DataSet
{
	std::string DecryptedMessage;
	UINT32 NumberofDictWords;
	UINT32 Sizeofallwords;
	UINT32 SizeofDictWords;
	UINT32 SizeofMessage;
	FLOAT64 score;
} DataSet;

// Initialize Text Analysis.
void InitTextAnalysis(void);

// Start Text analysis.
BOOL TextAnalysis(const std::string& Message);

#endif  //#ifndef TEXTANALYSIS_H