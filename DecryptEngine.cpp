/*
Name :  DecryptEngine.cpp
Author: Amit Malyala 
License: 
Copyright <2016> <Amit Malyala>, All rights reserved. 
Please see license.txt for details.
Description:
This program tries to decrypt a message which contains english text encrypted with XOR encryption with a unknown key by 
generating all possible keys to decrypt the message to obtain original message.Each message that is generated with generated 
key is then parsed to gather all possible words which are in that message. For each decrypted message, a score is calculated based 
on how many dictionary words are contained in the decrypted message compared to the size of the decrypted message.Each word 
is checked with a dictionary database. The decryption continues until all keys are generated and scores of all decrypted messages are 
compared.The maximum word length in each decrypted messsage is upto nineteen bytes. Each decrypted message with its number of dictionary words,
score and other parameters would form a dataset. The decrypted message with highest score would be selected as a possible original message.
In this project, only four char upper or lowercase alphabetic keys which decode the messsage can be detected.
The original message should contain monstly english words.
Comments are used in debug or development versions of the project.

Notes:
Implement the project first with four char alphabetical key then use random unicode keys.

Optional features to be implemented:
Test performance with openmp or std::thread
Implement a version which does CRC or hash Checks because non ECC Ram might be prone to errors. CRC checksum will be calculated for
each block of bytes read/written.

Known Issues:
None
Unknown issues:
None

Estimated effort:
------------------
SRS - 10 hours
SDS - 10 hours 
STS - 5 hours 
Defining Software architecture - 10 hours
Deciding on storage classes such as static and extern functions or variables,linkage of modules -  3 hours
Encryption of string or vector - 30 minutes
Decryption of string or vector - 30 minutes
Key generator which generates strings from - to ---- with characters 'a' to 'z' and 'A' to 'Z' - 8 hours
Decrypt Engine - Main control loop to call all modules , iterate through array of vector of strings for text parser - 10 hours
Text parser which parses words and adds them to a array of vector of strings for a set of generated keys - 10 hours
Dictionary module which looks up strings in a vector of strings containing dictionary words - 8 hours
TextAnalysis processes parsed words and does dictionary lookups . This module calculates the score of each array of vector of strings and keeps one, deletes the rest   - 10 hours
Progress bar to indicate progress of execution of the program - 1 hours
Conversion of generic data types to user defined data types - 5 hours

Coding log:
---------------

17-05-2016 Effort estimate written down , project compiles with G++ under C++11
           Key generator code being developed.
18-05-2016 Keygenerator code being developed.
20-5-2016  Wrote algorithm to generate a character at each character position. These characters are appended at each 
           character position. Implemented the key generator which contains 4 char alaphabetical keys.
25-5-2016  Added Encrypt Message and Decrypt Message functions. Integrated encryption, decryption with keygen to decrypt messsages 
           for all generated four char alphabetical keys in a control loop.
25-05-2016  Parser being developed which detects words or substrings.
28-05-2016 Completed word parser which extracts words from a string and adds them to a vector.
28-05-2016 Dictionary module being coded . Load a dictionary of words into a vector and sort them. When this dictionary function is called 
           a dictionary lookup is performed and the funtion returns a found or not found boolean value.
29-05-2016 Completed Dict module.
29-05-2016 Text analysis module being developed.
31-05-2016 Added datasets to the TextAnalysis module.Improved parser module to detect words at end of the message and between separators ,:.' '
3-06-2016  Wrote code in TextAnalysis function to check if each character in the middle of all words in a message are in uppercase. This is to check 
           if any of the messages incorrectly show themselves as original message.
03-06-2016 Completed text analysis module, Calibrating the module for various inputs and doing validation tests.           
04-06-2016 Changed TextParser module to allow detection of single word messages.           
04-06-2016 Changed code in sorting of datasets in TextAnalysis Component
05-06-2016 Changed keygenerator code to remove the while loop.
08-06-2016 Change parser to have a separate block for case ' ':, added a vector clear statement to initialize the parser
11-06-2016 Changed Text analysis component function to detect allow all capital words in a messsage 
12-06-2016 Changed some component functions in Dict, Keygen, TextParser modules to static
13-06-2016 Changed native data types to custom data types or type aliases in all modules.

Revision and bug history:
0.1 Initial version
0.2 Added encryption and decryption
0.3 Added key generator
0.4 Added Text parser
0.5 Added Dictionary module
0.6 Added Text Analysis module.
*/
#include <iostream>
#include <thread>
#include <chrono>
#include "DecryptEngine.h"
#include "Input.h"

// std::thread for chrono functions
using std::thread;
// Function main which contains the control loop.
SINT32 main(void) 
{
	DecryptEngine();
	return 0;
}

/*
Component Function: void DecryptEngine(void)
Arguments: None
Returns: None
Decription: Does decryption and prints original message 
or a message not found.
*/
void DecryptEngine(void)
{
	UINT64 i=0;
	bool OriginalMessageFound= false;
	std::string OriginalMessage ;
	std::string key ;
	std::string CipherMessage ;
	std::string DecryptedMessage ;
	std::string keystring;
	//GetInput(OriginalMessage);
	std::cout<<"This program decrypts a unknown message with XOR encryption"<< std::endl;
	// Initialize text analysis and dictionary modules.
	std::cout<<"Initializing Dictionary and Text analysis modules" << std::endl;
	InitTextAnalysis();
	//OriginalMessage = "Hello world";
	// Read an external file into Original Message
	readFile(OriginalMessage);
	key = "YbZY"; 
	std::cout <<"\nOriginalMessage: "<< OriginalMessage;
	std::cout<<"\nEncrypting message ";
	CipherMessage=EncryptMessage(OriginalMessage,key);
	std::cout <<"\nEncrypted Message is: " << CipherMessage;
	
	std::cout<<"\nDecrypting message ";
	// Generating all four char alphabetical keys which can decrypt the original message. Extended this control loop to support 8 char alphanumeric 
	// and other keys too.
	// Our control loop will iterate though all generated keys which can decrypt the orignal message
	// More functionality to be added in this loop. 
	while(i<=7454980 && OriginalMessageFound == false)
	{
  		// Generate a unique key for each iteration
  		keystring=Generatekey(i);
		// sleep for 10 microseconds 
		std::this_thread::sleep_for (std::chrono::nanoseconds(10000));
		//std::cout << "Key generated: " << keystring << std::endl;
		
		DecryptedMessage=DecryptMessage(CipherMessage,keystring);
		
		// sleep for 10 microseconds
		std::this_thread::sleep_for (std::chrono::nanoseconds(10000));
		//std::cout <<"\nDecrypted Message is: " << DecryptedMessage;
		 //sleep for 10 microseconds
		std::this_thread::sleep_for (std::chrono::nanoseconds(10000));
		// Call Text Analysis module here which would call the parser and dict modules.
		
		OriginalMessageFound=TextAnalysis(DecryptedMessage);
		//sleep for 10 microseconds
		std::this_thread::sleep_for (std::chrono::nanoseconds(10000));
		// Indicate progress of program execution.
		if (((i+1)%(745498) == 0) || i==0)
		{
			std::cout << ".";
		}
		i++;
		//sleep for 10 microseconds
		std::this_thread::sleep_for (std::chrono::nanoseconds(10000));
	}
	if(OriginalMessageFound)
	{
		std::cout<< "\nOriginal message is:"<< std::endl << DecryptedMessage;
		std::cout<< "\nKey genereated was "<< keystring << std::endl;
		std::cout << "\nDecryption complete" << std::endl;
	}
	else
	{
		if (DecryptedMessage != "")
		{
			std::cout<<"\nOriginal message not found"<< std::endl;
			
		}
		
		else
		{
			std::cerr<<"\nOriginal Message is empty";
		}
	}
}