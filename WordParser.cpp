/*
Module Name : WordParser.cpp
Author: Amit Malyala
License: 
Copyright <2016> <Amit Malyala>, All rights reserved. 
Please see license.txt for details.
Description:
This module contains a word parser function which will extract all words or strings separated
by or ending in !@#$%^?&*()_|~",'.=+-[]{};:/ or numbers 0123456789 in each decrypted message. These strings are added to a vector.
Version: 0.2 
*/

#include <iostream>
#include "Wordparser.h"
#include <vector>
#include <chrono>

// Uncomment to test this module.
// Comment to integrate with a project.
#if(0)
int main(void)
{
	//#if(0)
	std::string OriginalMessage = "This is a program. You can enter special characters as $^@&* , numbers 0123456789 and extended: ascii \
	characters such as ®¼Ð and äÆÆ.Write code to detect words or substrings seperated by or ending in !@#$%^?&*()_|~,'.=+-[]\{};:/ characters.";
	//#endif
	std::vector <std::string> WordList;
	//std::string OriginalMessage = "This is a bear";
	//std::cout << "Enter a message: ";
	//GetInput(OriginalMessage);
	//std::cout << "You entered: " << OriginalMessage;
	WordParser(OriginalMessage,WordList);
	
	//std::cout <<"\nOriginal message length " << OriginalMessage.length();
	return 0;				
}
#endif
/*
Component Function: void WordParser(const std::string& str);
Arguments: String which is to be parsed, vector which stores all substrings in the string.
returns : None
Description
Function to detect words or substrings seperated by or ending in !@#$%^?&*()_|~",'.=+-[]{};:/ characters or 
numbers 0123456789 or other extended ascii characters.
Version and bug history : 
0.1 Initial version.
0.2 Added support for word separators ,.:?-
0.3 Corrected a bug which was parsing incorrectly at the end of message.
*/
/* Substring parsing code begins */
void WordParser(const std::string& str, std::vector <std::string>& WordList)
{
	
	// Measure function execution time.
	//auto start = std::chrono::steady_clock::now();
	std::string substring;
	UINT32 i=0;
	// Clear and initialize Word list 
	WordList.clear();
	UINT32 StrLength=str.length();
    while (i<=StrLength)
    {    	
    /*
		 Use a switch statement here to detect alphabetical and !@#$%^?&*()_|\~`",'.=+-[]{};:/ characters or 
        numbers 0123456789
        Detect only alphabetical strings between ' ' or any of the characters or numbers above and before \0
    */
    switch(str[i])
    {
    	// Reading alphabetical chars	
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
		case 'g':
		case 'h':
		case 'i':
		case 'j':
		case 'k':
		case 'l':
		case 'm':
		case 'n':
		case 'o':
		case 'p':
		case 'q':
		case 'r':
		case 's':
		case 't':
		case 'u':
		case 'v':
		case 'w':
		case 'x':
		case 'y':
		case 'z':
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
			//std::cout <<"Reading a Alphabetical char"<< std::endl;
			substring += str[i];
			i++;
			break;
		// reading special chars
		//case '!':
	    case '@':
	    case '#':
	    case '$':
	    case '%':
		case '^':
		//case '?':
		case '&':
		case '*':
		case '(':
		case ')':
		case '_':
		case '|':
		case '\\':
		case '~':
		case '`':
		case '"':
		case '>':
		case '<':	
		case '\'':
		case '=':
		case '+':
		case '[':
		case ']':
		case '{':
		case '}':
		case ';':
		//case ':':
		case '/':
		case '\n':
			
		//std::cout <<"Reading a Special char"<< std::endl;
			i++;
			break;
		// Now reading digits
		case '0':
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			//std::cout <<"Reading a decimal"<< std::endl;
			i++;
			break;
			
		// Reading a space before or after a word
		case ' ':
			//std::cout <<"Reading a ' '"<< std::endl;
			if (substring != "")
			{
				// Add each extracted word to a vector
				WordList.push_back(substring);
				
			}
			substring ="";
			i++;
			break;	
		// Reading a , . : ? - ! before or after a word.
		case ',':
		case '.':
		case '?':
		case '!':
		case ':':
		case '-':	
    		//std::cout <<"Reading a ? , . : - "<< std::endl;
			if (substring != "")
			{
				// Add each extracted word to a vector
				WordList.push_back(substring);
				
			}
			substring ="";
			i++;
			break;	
		case '\0':
			//std::cout <<"Reading end of string \0"<< std::endl;
			// Add each extracted word to a vector
			if (substring != "")
			{
				// Add each extracted word to a vector
				WordList.push_back(substring);
				
			}
			i++;
			break;
			
		// Reading other characters which are possibly extended ascii chars.	
		default:
			//std::cout <<"Reading a extended ascii char"<< std::endl;
			i++;
			break;
	}
   } // End of while
   // parsing code ends
   // Comment #if and #endif if running a full version, uncomment to run the development version.
   #if(0)
    //system("pause");
   system("pause");
    
   if (WordList.size())
   {
   	  std::cout<< "Number of words in decrypted message "<< WordList.size()<< std::endl;
   }
   // Display list of words found in the message for debugging
   //std::cout<< "Displaying list of words in decrypted message" << std::endl;
   for (UINT32 j=0;j<WordList.size();j++)
   {
   	std::cout <<WordList[j]<<std::endl;
   }
   
   #endif 
  // Measure execution time.
  //auto end = std::chrono::steady_clock::now();
  //auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
  //std::cout << "Execution time " << diff << " nanoseconds";
}