/*
Name :  Keygen.cpp
Author: Amit Malyala
License: 
Copyright <2016> <Amit Malyala>, All rights reserved. 
Please see license.txt for details.
Description:
Key generator which generates strings from - to ---- with characters 'a' to 'z' and 'A' to 'Z'.
There are about 52*52*52*52 possible keys.

To do:
Change keygenerator to generate keys with all ascii symbols with a key of 64 bytes length.
*/
#include <iostream>
#include <string>
#include <cstdlib>

#include "Keygen.h"
// Get First char
// First char changes at each iteration and repeats every 52 iterations.
static SINT8 GetFirstChar(char& l);
// Get Second char
// Second char changes every 52 interations and repeats itself every 52*52 iterations.
static SINT8 GetSecondChar(char& m);
// Get third char
// Third char changes every 52*52 iterations and repeats itself every 52*52*52 iterations.
static SINT8 GetThirdChar(char& n);

// Fourth char changes every 52*52*52 iterations and repeats itself every 52*52*52*52 iterations.
// Get Fourth char
static SINT8 GetFourthChar(char& p);

#if(0)
// Move this code to the main control loop 
// Uncomment to test this module
int main(void)
{
	long int i=0;
	std::string keystring;
    while(i<=7454980)
  	{
		keystring=Generatekey(i);
		i++;
	}
	return 0;
}
#endif 

/*
Function Name :  std::string  Generatekey(UINT64 &Count)
Date : 17-05-2016
Description:
This function genreates keys using first, second, third and fourth characters.
Write code here for generating first, second, third and fourth character of the key and append them to key string 
ascii codes 65-90 A-Z,  97-122 a-z
Number of iterations required to change character at each position.
Characters change from a-z and A-Z in that order.
One char                        1 - 
Two char                    52  1 - 
Three char            52x52 52  1 - 
Four char   52x52x52  52x52 52  1 - 
Use four while loops or four if-else conditional statements?
Remember that strings add bytes to the right.Newly added characters would be added on right.
The order of writes would be  l, ml , nml,  pnml if lmnp is the order of characters
MS byte is filled first and LS byte is filled last.
Watch out for function call stack when each function calls other functions.
*/
std::string  Generatekey(UINT64 &Count)
{
	static SINT8 l=97,m=97,n=97,p=97;
	std::string key;
    if (Count<52)
	{
	   key = GetFirstChar(l);
	    //std::cout << "Key generated: " << key << std::endl;
	}
	else if (Count>=52 && Count <2756)
	{
	   key = GetSecondChar(m);
	   key+=GetFirstChar(l);
	   //std::cout << "Key generated: " << key << std::endl;
	}
	else if (Count>=2756 && Count <143364)
	{
	   key=GetThirdChar(n);
	   key+=GetSecondChar(m);
	   key+=GetFirstChar(l);
	   //std::cout << "Key generated: " << key << std::endl;
	}
	else if (Count>=143364 && Count< 7454980)
	{
	   key=GetFourthChar(p);
	   key+=GetThirdChar(n);
	   key+=GetSecondChar(m);
	   key+=GetFirstChar(l);
	   //std::cout << "Key generated: " << key << std::endl;
	}
    return key;
}
/*
Function to generate first char
This character changes at each iteration. 
Characters a-z and A-Z are generated.
Each character repeats itself after 52 iterations.
Each character changes after 1 iteration.
Component can be modified to support character generation for any position at runtime.
*/
static SINT8 GetFirstChar( char& l)
{
static int x=0;	
register char FirstChar = ' ';
// code for generating one char i.e first char
	if (x==0)
	{
		l=97;
		FirstChar=l; // store first char
	}
				
	else if (x <26|| x>26)
	{
	    FirstChar = l;
	}
	else if (x== 26)
	{
	  	l=65;
	  	FirstChar=l;
	}
				
	l++;
	x++;
	if (x==52)
	{
		x=0;
		l=97;
	
    }
	return FirstChar;
}
/*
Component function:  GetSecondChar(char& m)
Function to generate second char
Characters a-z and A-Z are generated.
Each character changes every 52 iterations.
Each character repeats itself after 52*52 iterations.
*/
static SINT8 GetSecondChar( char& m)
{
  static long int x=0; // Keep track of number of iterations at which char changes.
  static long int y=0; // Keep track when char changes.
  register char SecondChar = ' ';
  // code for generating one char i.e second char
	if (x==0)
	{
		m=97;
		SecondChar=m; // Store third char
	}
	else if (x <26*52|| x>26*52 )
	{
	    SecondChar = m;
	}
	else if (x== 26*52)
	{
	  	m=65;
	  	SecondChar=m;
	   	
	}
    x++;
	if (x%52==0)
	{
		m++;
		y++;
		//system("pause");
	}
	//#if(0)
	if (y==52)
	{
		x=0;
		y=0;
	}
	//#endif
	return SecondChar;
}

/*
Function to generate Third char
Characters a-z and A-Z are generated.
Each character changes every 52*52 iterations.
Each character repeats itself after 52*52*52 iterations.
*/
static SINT8 GetThirdChar( char& n)
{
  static long int x=0; // Keep track of number of iterations at which char changes
  static long int y=0; // Keep track when char changes.
  char ThirdChar = ' ';
  // code for generating third char
  if (x==0)
  {
	n=97;
	ThirdChar=n; // store third char
  }
  else if (x <26*52*52|| x>26*52*52)
  {
	ThirdChar = n;
  }
  else if (x== 26*52*52)
  {
	n=65;
	ThirdChar=n;
  }
  x++;
  if (x%(2704)==0)
  {
	n++;
	y++;
  }
  if (y==52)
  {
	x=0;
	y=0;
	//system("pause");
  }
  return ThirdChar;
}
/*
Function to generate Fourth char
Characters a-z and A-Z are generated.
Each character changes every 52*52*52 iterations.
Each character repeats itself after 52*52*52*52 iterations.
*/
static SINT8 GetFourthChar(char& p)
{
  static long int x=0; // Keep track of number of iterations at which char changes
  static long int y=0; //Keep track when a char changes.
  register char FourthChar = ' ';
  // code for generating Fourth char
  if (x==0)
  {
	p=97;
	FourthChar=p; // Store Fourth char
  }
  else if (x <26*52*52*52|| x>26*52*52*52)
  {
	FourthChar = p;
  }
  else if (x== 26*52*52*52)
  {
	p=65;
	FourthChar=p;
  }
  x++;
  if (x%(140608)==0)
  {
	p++;
	y++;
  }
  if (y==52)
  {
	x=0;
	y=0;
  }
  return FourthChar;
}
