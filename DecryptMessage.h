/*
Name :  DecryptEngine.h
Author: Amit Malyala
Date : 12-05-2016
Description:
Header file which has references to function prototypes of other modules.
*/
#ifndef DECRYPTENGINE_H
#define DECRYPTENGINE_H

#include "EncryptMessage.h"
#include "DecryptMessage.h"
#include "Keygen.h"
#include "TextAnalysis.h"
/*
Component Function: void DecryptEngine(void)
Arguments: None
Returns: None
Decription: Does decryption and prints original message 
or a message not found.
*/
void DecryptEngine(void);

#endif 