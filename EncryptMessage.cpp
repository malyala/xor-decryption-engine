/*
License: 
Copyright <2016> <Amit Malyala>, All rights reserved. 
Please see license.txt for details.
*/
#include "EncryptMessage.h"

// Function to encrypt a message
std::string EncryptMessage(const std::string& Message,const std::string& key)
{
	SINT32 Keylength= key.length();
	std::string CipherMessage=Message;
	SINT32 Messagelength= CipherMessage.length();
	int i=0,j=0;
	for (i=0,j=0;i<Messagelength;i++,j++)
	{
		if (j == Keylength)
		{
			j=0;
		}
		CipherMessage[i] = Message[i]^key[j];
    }
    return CipherMessage;
   
}

