/*
Module Name :  Dict.cpp
Author: Amit Malyala
Date : 25-05-2016
Description:
The dictionary module loads a dictionary and then enables search of word strings in the dictionary which is a vector of strings.
Initialize the dictionary as a vector of word strings, sort this vector if that isn't sorted and do binay 
search to find a word string in the dictionary.Do this for each word string that is supplied for lookup.
*/
#include "dict.h"
#include "iostream"

// Vector for storing dictionary words
static std::vector <std::string> Dictionary;
// Read contents of a filename and store that dictionary vector WordList
static void BuildDictionary(std::string& filename, std::vector <std::string> & WordList);
// Uncomment to test this module.
// Comment to integrate with a project.
#if(0)
int main(void)
{
	bool WordFound=false;
	// Define this variable in main or TextAnalysis module.
	// Define this as extern for access in different modules?
	static std::vector <std::string> WordList;
	
	// Initialize dict in main module
	InitDict();
		
	std::string word ="bear";
	// Search a word in a dictionary
	WordFound=SearchDict(word);
	if (WordFound)
	{
		std::cout <<"Found word " << word;
	}
	return 0;				
}
#endif
/*
Component Function: InitDict(std::vector <std::string>& WordList)
Arguments: Wordlist vector of strings to build a dictionary of words
returns : Word found or not found
Version : 0.2
Description:
Initialize dictionary , load dictionary word strings from a file into a vector and sort that vector.
Commented lines of code are used in development/debug version.
*/
void InitDict(void)
{
	std::string filename = "wordsen.txt"; //Wordlist from SIL international
	Dictionary.clear();
	BuildDictionary(filename, Dictionary); 
	
}
/*
Component Function: void BuildDictionary(std::string& filename, std::vector <std::string>& WordList)
Arguments: Filename, Wordlist vector of strings to build a dictionary of words
returns : None
Version : 0.2
Description:
Read and display contents of a filename to build a dictionary
Use this file for building the dictionary module. Load a word list into a vector of strings with this function
Commented lines of code are used in debug version.
*/

static void  BuildDictionary(std::string& filename, std::vector <std::string>& WordList)
{
  //UINT32 i=0;
  //UINT32 j=0;
  std::string line;
  std::ifstream Dictfile (filename);
  //std::string word;
  //char c;
  if (Dictfile.is_open())
  {
    //while (myfile.get(c ))  // For reading bytes
    while (getline(Dictfile,line))
    {
      // std::cout << c ; // Printing bytes
       // Other way of adding string to the vector, only used during development.
	   //for (i=0;line[i]!='\0';i++)
      // {
       	   //word+=line[i];
      // }
      //std::cout << line << std::endl ; // Print a line
      //WordList.push_back(word);
      WordList.push_back(line);
      //std::cout << word << std::endl ; // Print a line
      //std::cout << WordList[j]<<std::endl; // Print a line
      //word="";
      //j++;
    }
    Dictfile.close();
  }

  else
  {
      //std::cout << std:: endl << "File not found";
  }
}

/*
Component Function:BOOL SearchDict(std::string& key)
Arguments: string to be searched
returns : Word found or not found
Version : 0.2
Description:
Search for a word string in a vector of strings 
Commented lines of code are used in debug version.
Function from Kernighan and Ritchie, C programming language, 1990.
*/
BOOL SearchDict(const std::string& key)
{
   BOOL WordFound=false;
	// Do a binary search in the vector of strings to search for  a word string.
   UINT32 count= Dictionary.size();	
   UINT32 low = 0;
   UINT32 high = count-1;
   UINT32 mid = 0;
   while(low <= high && WordFound==false)
   {
   	 mid = (low+high)/2;
     if(key < Dictionary[mid])
     {
        high = mid-1;
        //std::cout <<"Word is less than vector element" << std::endl;
     }
     else if(key > Dictionary[mid])
     {
        low = mid+1;
        //std::cout <<"Word is greater than vector element" << std::endl;
     }
     else
     {
        WordFound=true;
     }
   }
    return WordFound;
}

/*
Component Function: BOOL LinearSearchDict(const std::string& key)
Arguments: Dictionary of vector strings, string to be searched
returns : Word found or not found
Version : 0.2
Description:
Search for a word string in a vector of strings 
Commented lines of code are used in debug version.
*/
BOOL LinearSearchDict(const std::string& key)
{
   BOOL WordFound=false;
	// Do a binary search in the vector of strings to search for  a word string.
   UINT32 count= Dictionary.size();	
   UINT32 i=0;
  
   //std::cout << "Key string is " << key<< std::endl;
   for (i=0;i<count && WordFound==false;i++)
   {
       if (key==Dictionary[i])
	   {
	   	   WordFound=true;
	   	   //break;
	   }   	
   }
    return WordFound;
}