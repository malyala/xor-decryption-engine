/*
License: 
Copyright <2016> <Amit Malyala>, All rights reserved. 
Please see License.txt for details.
*/
#ifndef ENCRYPTMESSAGE_H
#define ENCRYPTMESSAGE_H

#include "std_types.h"
#include "iostream"
#include "string"
#include "fstream"

// Function to encrypt a message
std::string EncryptMessage(const std::string& Message, const std::string& key);

#endif  // #ifndef ENCRYPTMESSAGE_H