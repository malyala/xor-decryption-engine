/*
Module Name :  TextAnalysis.cpp
Author: Amit Malyala
Date : 25-05-2016
License: 
Copyright <2016> <Amit Malyala>, All rights reserved. 
Please see license.txt for details.
Description:
The text analysis module calls the text parser and dictionary modules. Returns a message found or not found to the caller.
Known issues:
Single letter messages should be more than 6 characters to meet variation of encryped messages with four char keys.More than two words in a message 
are required with larger keys. 
Notes:
Implement a version in which all writes and reads to RAM are done with ECC in software. Add a checksum or CRC for writing/reading each block of bytes.
This may involve reading twice or read after write to RAM.

To do:
Change dataset score analysis. 

*/

#include "TextAnalysis.h"
#include "iostream"
#include "algorithm"
#include <cctype>

// This vector creates our datasets
static std::vector <DataSet> Data;
/*
Component Function: void SortDatasets(void)
Arguments: None
returns : Nothing.
Description:
Vector which contains datasets would be sorted by their score.
Version : 0.1 Initial version
*/
static void SortDataSets(void);

// This vector is used to extract parsed words from string DecryptedMessage 
static std::vector <std::string> WordList;

/* Component function: SINT8 chartolower(SINT8 str)*/
SINT8 chartolower(SINT8 str);
/*
Component Function: void InitTextAnalysis(void)
Arguments: None
returns : None
Description:
Initializes text analysis and dictionary modules.
Version : 0.2
*/

void InitTextAnalysis(void)
{
	UINT32 c=0;
	//Initialize the vector by emptying 
	WordList.clear();
	// Initialize the dictionary module
	InitDict();
	// Create a null dataset
	DataSet Input={"",0,0,0,0,0};
	//  Initialize datasets vector
	for (c=0;c<10;c++)
	{
		Data.push_back(Input);
		
	}
	
}
/*
Component Function: BOOL TextAnalysis(const std::string& Message)
Arguments: Message String which is to be analyzed
returns : Message is orignal message or not
Description:
Write code here which will parse each Decrypted message. Extract words from that, look up each 
extracted word in the dictionary module. Calculate a score for each message based on message length, 
number of dictionary words found in the message.
Optimize this function and other functions it is calling for greater than 10000 calls per second.
              
Version and bug history:
0.1 Initial version
0.2 Added lookup of dictionary
0.3 Added entries for calculating number of all words, number of dict words, score
0.4 Added code to check if any characters in the middle of a word in a message are in uppercase.
0.5 Added code to sort datasets and predict original message using statistical analysis of datasets.
*/
BOOL TextAnalysis(const std::string& Message)
{
	bool MessageFound=false;
	UINT32 i=0;
	UINT32 NumberofWords=0;
	UINT32 NumberofDictWords=0;
    UINT32 SizeofAllWords=0;
    UINT32 SizeofDictWords=0;
    UINT32 SizeofMessage=0;
    FLOAT64 Score=0.0;
	std::string Searchstring;
	UINT8 CapitalsCount=0;
	UINT32 SearchStrLength =0;
	UINT32 j=0;
	static UINT32 DataSetCount=0;
	// Initialize the vector
    WordList.clear(); 
	//Call Text parser
    //Extract Words from TextParser
    //Calculate total number of alphabetical word strings in the wordlist
	WordParser(Message,WordList);
	// Number of alpabetical words in a parsed message
	NumberofWords = WordList.size();
	//#if(0)	
   if (NumberofWords)
   {
   	  //std::cout<< "Number of words in decrypted message "<< NumberofWords<< std::endl;
   	  //std::cout<< "Displaying list of words in decrypted message" << std::endl;
   	  //system("pause");
      for (i=0;i<NumberofWords;i++)
      {
   	    //std::cout <<WordList[j]<<std::endl;
   	    // Check string endings.
		Searchstring=WordList[i];
		SearchStrLength= Searchstring.length();
		SizeofAllWords+=SearchStrLength;
   	    //std::cout<<"Search string is " << Searchstring << std::endl;
   	    
   	    // Check if a word string in a message has any capital letters in the middle.
   	    // If there are capital letters a positon other than first position of a word then ignore that messsage and read the next message.
   	    // Allow all capital dictionary words or other words in a message.
   	    
		// Modified block of code
		//Optimize this block of code.
		//#if(0)
		/* 
		Either a word should be all capitals or start with a capital. A word cant start with a lowercase letter and have capitals 
		in the middle.
		This is important because xor decryption sometimes gives nearly correct decryption with random upper or lowercase letters in each word
		with keys which are near the original key.
		
		*/
		if (isupper(Searchstring[0]))
		{
			for (j=0;j<SearchStrLength;j++)
   	        {   
   	    	  if (isupper(Searchstring[j]) )
   	    	  {
   	    	  	CapitalsCount++;   	    	  	
   	    	  	
			  } 
		    }
			
		}
	
	    if (CapitalsCount != SearchStrLength)
	    {
	    	for (j=1;j<SearchStrLength ;j++)
   	        {   
   	    	  if (isupper(Searchstring[j]) )
   	    	  {
   	    		MessageFound=false;
   	    		return MessageFound;
			  } 
		    }
		}
		
		CapitalsCount=0;
		//#endif 
		// Modified block of code ends
   	    // Convert search string to lowercase
		// std::transform(Searchstring.begin(), Searchstring.end(), Searchstring.begin(), ::tolower);
   	    std::transform(Searchstring.begin(), Searchstring.end(), Searchstring.begin(),chartolower);
   	    // Look up words which are 2 chars or more in size.
		if( SearchStrLength >=2)
		{
			if (SearchDict(Searchstring))
			{
				NumberofDictWords++;
				SizeofDictWords+=SearchStrLength;
				//std::cout<<"Found dictionary word " << Searchstring << std::endl;
				//std::cout << "Dictionary words " << NumberofDictWords<< std::endl;
   	    	}
		}
      }
      // Dictionary look up works fine.
	  // Modify this block with a probabilistic algorithm.
      //Modification block begins
      
      // SizeofMessage
	  SizeofMessage=Message.length();
	  // Score of each message.Keep the message with highest score.
	  Score= (FLOAT64)SizeofDictWords/(FLOAT64)SizeofMessage;
	  
     // Check if number of dict words are equal to total number of words. Also score should be greater than 0.4.
      if (NumberofWords==NumberofDictWords && Score>0.5)
      {
      	
		  MessageFound=true;
		  //std::cout<<"Message found "<< Message<< std::endl;
		  //std::cout<<"\nScore is " << Score << std::endl;
		  //std::cout<<"Size of all words " << SizeofAllWords << std::endl;
		  //std::cout<<"Size of Dict words "<<SizeofDictWords << std::endl;
		  //std::cout<<"Size of Message "<< SizeofMessage << std::endl;
	  }   
	  // Dataset accumulation and sorting code 
	  else if (Score >0)
	  {
	  	// If there are non dictionary words in the message, this block would make sure such messages are analyzed.
	  	// Accumulate upto 10 datasets and then sort them. 
	  	// Keep one dataset
	  	// More parameters of each dataset added for debugging.
	  	// Dataset code to be included in sorting of datasets for messages with short number of dictionary words.
	  	//std::cout<<"\nCreating datasets" ;
		Data[DataSetCount].score=Score;
	  	Data[DataSetCount].DecryptedMessage=Message;
	  	Data[DataSetCount].NumberofDictWords=NumberofDictWords;
	  	Data[DataSetCount].SizeofMessage=SizeofMessage;
	  	Data[DataSetCount].SizeofDictWords=SizeofDictWords;
		//Sort the datasets now to have the highest score dataset as last element.
		SortDataSets();  
		//Display first element of the vector.
		//std::cout<<"\nScore in last element of vector is" << Data[9].score ;
		//std::cout<<"\nDecryptedMessage in last element of vector is " << Data[9].DecryptedMessage << std::endl;
	     DataSetCount++;	
	     
	    if (DataSetCount == 10)  //There are not many comparisions, so sort before you have many datasets accumulated.
		{
			DataSetCount=0;
			
			//#if(0)
			UINT32 c=0;
			DataSet EmptyDataSet = {"",0,0,0,0,0};
			// Keep the last dataset and write the vector upto the last dataset. Sort again, repeat.
			for(c=0;c<9;c++)
			{
				Data[c]=EmptyDataSet; // Empty all elements from second to last in the vector
			}
			//#endif
	    }
	    
	    if (NumberofDictWords >=1 && NumberofWords>=1 && Data[9].score>=0.3)
	    {
	    	MessageFound=true;
	    	//return MessageFound;
	    	//std::cout<<"\nNumber of Dict words"<< NumberofDictWords;
	    	//std::cout <<"\nScore of the message: "<<Score;
	  	}
	
	  }
	    // Dataset accumulation and sorting code  ends
        //system("pause");
	    //std::cout<<"Size of all words " << SizeofAllWords << std::endl;
	    //std::cout<<"Size of Dict words"<< SizeofDictWords << std::endl;
	    //std::cout<<"Size of Message" << SizeofMessage << std::endl;
	    // Modification block ends
   }
   //#endif 
   //std::cout<< "Number of words in decrypted message "<< NumberofWords<< std::endl;
   // std::cout<< "Number of dictionary words "<< NumberofDictWords << std::endl;
   // std::cout<< "Text analysis called " << Textanalysiscount << std::endl;
   	return MessageFound;
}

/*
Component Function: void SortDatasets()
Arguments: None
returns : Nothing.
Description:
Vector which contains datasets would be sorted by their score.
Version : 0.1 Initial version
*/
static void SortDataSets()
{
	// Sort the vector
	// C++ 11 lambda function
	std::sort(Data.begin(), Data.end(), [](const DataSet& a, const DataSet& b) { return a.score < b.score; }); 
}

/* Component function: chartolower(SINT8 str)*/
SINT8 chartolower(SINT8 str)
{
	return tolower(str);
}